#!/usr/bin/env python3

from setuptools import setup
from setuptools import find_packages

setup(
    name="pythondata-peripheral-opencoresi2c",
    description="Verilog files for OpenCores I2C module",
    author="Raptor Engineering, LLC",
    author_email="support@raptorengineering.com",
    url="https://www.raptorengineering.com",
    download_url="https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-litex/pythondata-peripheral-opencoresi2c",
    test_suite="test",
    license="BSD",
    python_requires="~=3.6",
    packages=find_packages(exclude=("test*", "sim*", "doc*", "examples*")),
    include_package_data=True,
)
